package com.docker.DockerExample;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DockerExampleApplicationTests {

	@Test
	public void contextLoads() {
	}
	@Test
	public void test() {
		assertEquals("abc","abc");
		System.out.println("Running Tests");
	}

}
